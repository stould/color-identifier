1200 input data obtained from a sensor: 300 red, 300 blue, 300 green, 300 yellow.


The first line of an input data is for example: 10 15 20

The second line is the class in format: 1, -1, -1, -1 (all positions -1, except by the class index which is 1)

classes for each color:

red = 1 -1 -1 -1

green = -1 1 -1 -1

blue = -1 -1 1 -1

yellow = -1 -1 -1 1

This ANN (Perceptron) tries to guess for a new input. Good results were obtained, around 95% correct. Next step is test with MLP.