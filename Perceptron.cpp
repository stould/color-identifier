#include <bits/stdc++.h>
#define INF 0x3f3f3f3f


typedef long long ll;

using namespace std;

const int MAX_DIGITS = 4;

//input = 32 integers
//[0,15] = rows
//[16, 31] = cols
struct Data{
    int digit;
    vector<double> input;
    vector<int> output;
    Data(){}
    Data(int digit_, vector<double> &in, vector<int> &out){
        input = in;
        output = out;
        digit = digit_;
        //normalize((double) (1 << 16), 0.0);
    }
    Data(int digit_, vector<double> &in){
        input = in;
        digit = digit_;
    }
    void normalize(double MAX_, double MIN_){
        for(int i = 0; i < input.size(); i++){
            input[i] = (input[i] - MIN_) / (MAX_ - MIN_);
        }
    }
};

//split data by class [0, 9]
vector<Data> data[MAX_DIGITS];

//train and test data respectively
vector<Data> trainData[MAX_DIGITS], testData[MAX_DIGITS];

//degree activation function
int activationFunction(double y_out){
    if(y_out >= 1.0){
        return 1;
    }else{
        return -1;
    }
}

/*
    This function will set the training data as a percentage P
    of the total. The rest goes to test.
*/
void splitData(double P){
    for(int digit = 0; digit < MAX_DIGITS; digit++){
        random_shuffle(data[digit].begin(), data[digit].end());

        int training =  (int) ((P / 100.0) * (double) data[digit].size());
        int testing =  (int) (data[digit].size() - training);
        //P to traing
        for(int i = 0; i < training; i++){
            trainData[digit].push_back(data[digit][i]);
        }

        //(Total - P) to test
        for(int i = training; i < (int) data[digit].size(); i++){
            testData[digit].push_back(data[digit][i]);
        }

        if(testData[digit].size() + trainData[digit].size() != data[digit].size()){
            cout << "Something went wrong. Total != train + test" << endl;
        }
    }
}

/*
    dataSets = number of instances
    N = 256 -> input grid elements
    M = 10 -> expected answer elements
    width = image width
    height = image height
*/
void readAndParseData(int dataSets, int N, int M){
    ifstream infile;
    infile.open("semeion.data");

    int categoryId;
    double val;

    //read and parse until dataSets = 0
    while(dataSets > 0){
        vector<double> input;
        vector<int> output;

        categoryId = -1;
        //begin reading data
        for(int i = 0; i < N; i++){
            infile >> val;
            input.push_back(val);
        }

        for(int i = 0; i < M; i++){
            infile >> val;
            output.push_back(activationFunction((double)val));
            if(output[i] == 1){
                categoryId = i;
            }
        }
        if(input.size() != 3 || output.size() != 4 || categoryId == -1){
            printf("Input error %d %d %d\n", (int) input.size(), (int) output.size(), categoryId);
        }
        //end reading data

        data[categoryId].push_back(Data(categoryId, input, output));
        dataSets = dataSets - 1;

    }
    infile.close();
}

//reading a matrix of 16x16 without expected output
vector<Data> readRandomData(int inputs, string filename, int N){
    ifstream infile;
    infile.open(filename.c_str());
    vector<Data> ans;
    while(inputs){
        int categoryId;
        double val;

        vector<double> input;

        //begin reading data
        for(int i = 0; i < N; i++){
            infile >> val;
            input.push_back(val);
        }
        //end reading data
        inputs--;
        ans.push_back(Data(-1,input));
    }
    infile.close();
    return ans;
}

double dotProduct(vector<double> &x, vector<double> &w){
    double ans = 0.0;
    if(x.size() != w.size()){
        cout << "different input size... returning 0 as standard." << endl;
    }else{
        for(int i = 0; i < x.size(); i++){
            ans += x[i] * w[i];
        }
    }
    if(ans > 100000 || ans < -100000){
        cout << "Overflow -> "<< ans << endl;
    }
    return ans;
}

struct Perceptron{

    /*
        W = weights for each network
        bias = bias for each network
        alhpa = alpha for each network
        alpha_decaiment_rate = total rate of decaiment per training
    */
    vector<double> w;
    double bias;
    double alpha;
    double alpha_decaiment_rate;
    int digit;

    Perceptron(int digit_){
        w = vector<double>(3, 0.0);
        bias = 0.0;
        alpha = 0.4;
        alpha_decaiment_rate = 0.99999;
        digit = digit_;
    }

    /*
        digit = digit to be trained
        iterations = number of iterations
    */
    void trainNetwork(vector<Data> &dados, int iterations){
        while(iterations-- > 0){
            //random data
            int line = rand() % int(dados.size());

            vector<double> &input = dados[line].input;
            vector<int> &output = dados[line].output;
            double y_out = dotProduct(w, input) + bias;
            int y = activationFunction(y_out);
            int error = output[digit] - y;
            if(error != 0){
                for(int j = 0; j < input.size(); j++){
                    w[j] = w[j] + alpha * (double) error * input[j];
                }
                bias += alpha * (double) error;
            }
            alpha *= alpha_decaiment_rate;
        }
    }

    double testNetwork(vector<Data> &test){
        int correct = 0, total = test.size();
        for(int i = 0; i < test.size(); i++){
            vector<double> &input = test[i].input;
            vector<int> &output = test[i].output;
            double y_out = dotProduct(w,input) + bias;
            int y = activationFunction(y_out);
            int error = output[digit] - y;
            if(error == 0){
                correct++;
            }
        }
        return ((double)correct * 100.0) / (double) total;
    }

    int testNetwork(Data &test){
        vector<double> &input = test.input;
        vector<int> &output = test.output;
        double y_out = dotProduct(w,input) + bias;
        int y = activationFunction(y_out);
        return y;
    }
};

vector<Perceptron> network;

double tests(vector<double> &confiability, bool EXTRA){
    int test[4];
    int correct = 0, total = 0;
    for(int i = 0; i < 4; i++){
        int right = 0;
        total += testData[i].size();
        for(int j = 0; j < testData[i].size(); j++){
            bool can = 1;
            int recognizes = 0;
            //generating outputs for each network.
            for(int k = 0; k < 4 && can; k++){
                test[k] = network[k].testNetwork(testData[i][j]);
                recognizes += (test[k] == 1);
                if(test[k] != testData[i][j].output[k]){
                    can = 0;
                }
            }

            /*
                *giving to the network a chance*:

                1) From all networks that recognizes the digit,
                get the most confiable (greatest accuracy) network X.

                2) If X has a unique probability among other networks
                and expected_output[X] == 1, then increase the answer.

                ** If there are more than one network with the same confiability,
                then none will break the tie **
            */
            if(can == 0 && recognizes > 1 && EXTRA){
                int mostConfiable = -1;
                double mostConfiableValue = 0.0;
                for(int k = 0; k < 4; k++){
                    if(test[k] == 1){
                        if(confiability[k] > mostConfiableValue){
                            mostConfiableValue = confiability[k];
                            mostConfiable = k;
                        }
                    }
                }
                int cnt = 0;
                for(int k = 0; k < 4; k++){
                    if(mostConfiableValue == confiability[k]){
                        cnt++;
                    }
                }
                if(cnt == 1 && (testData[i][j].output[mostConfiable] == 1)){
                    right ++;
                }
            }else{
                right += can;
            }
        }
        correct += right;
        printf("color [%d] -> (%d, %d) = %.2f\n", i, right, (int) testData[i].size(), (right * 100.0) / (double) testData[i].size());
    }
    puts("");
    return (correct * 100.0) / total;

}

int guess(Data &data){
    int test[4];
    int totalActive = 0, ans = -1;
    for(int i = 0; i < 4; i++){
        test[i] = network[i].testNetwork(data);
        if(test[i] == 1){
            totalActive++;
            ans = i;
        }
    }
    if(totalActive == 1){
        return ans;
    }else{
        return -1;
    }
}

int main() {

    srand(time(0));
    readAndParseData(1200, 3, 4);

    vector<Data> randomData = readRandomData(24, "in.in", 3);

    splitData(60);//X% to training
    vector<Data> whole_tests;
    for(int i = 0; i < 4; i++){
        for(int j = 0; j < testData[i].size(); j++){
            whole_tests.push_back(testData[i][j]);
        }
    }
    vector<double> confiability;
    int tries = 10;
    int iterations = 5000;

    double totalOfTests = 0;
    for(int _ = 0; _ < tries; _++){
        confiability.clear();
        network.clear();
        for(int i = 0; i < MAX_DIGITS; i++){
            Perceptron p(i);
            network.push_back(p);
        }
        for(int i = 0; i < 4; i++){
            vector<Data> specialized_data;
            for(int j = 0; j < 4; j++){
                int len = trainData[j].size();
                int ammount = len;
                if(i != j){
                    ammount = (double) len * 0.4;
                    for(int k = 0; k < ammount; k++){
                        int pos = rand() % len;
                        specialized_data.push_back(trainData[j][pos]);
                    }
                }else{
                    for(int k = 0; k < ammount; k++){
                        specialized_data.push_back(trainData[j][k]);
                    }
                }
            }
            random_shuffle(specialized_data.begin(), specialized_data.end());
            network[i].trainNetwork(specialized_data, iterations);
            confiability.push_back(network[i].testNetwork(specialized_data));
        }
        double result = tests(confiability,0);
        totalOfTests += result;
        if(result > 94){
            for(int i = 0; i < randomData.size(); i++){
                int color = guess(randomData[i]);
                switch(color){
                    case -1:
                        cout << "Unknown" << endl;
                        break;
                    case 0:
                        cout << "Red" << endl;
                        break;
                    case 1:
                        cout << "Green" << endl;
                        break;
                    case 2:
                        cout << "Blue" << endl;
                        break;
                    case 3:
                        cout << "Yellow" << endl;
                        break;
                }
            }
        }
    }
    cout << "Mean of tests = " << totalOfTests /  (double) (tries) << endl;
    return 0;
}
